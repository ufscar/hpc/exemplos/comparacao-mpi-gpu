#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
    double inicio, fim;
    double t;
    int nA, nB;
    int size;
    int *A, *B, *C;
    int i;
    FILE *fC;
    FILE *fA = fopen("A", "r");
    FILE *fB = fopen("B", "r");
    fscanf(fA, " %d", &nA);
    fscanf(fB, " %d", &nB);
    if(nA != nB) {
        printf("ERRO: matrizes geradas incorretamente\n");
        exit(1);
    }

    size = nA*nA*sizeof(int);

    // inicializa as matrizes A e B
    A = (int*) malloc(size);
    B = (int*) malloc(size);
    C = (int*) malloc(size);
    for(i=0; i<nA*nA; i++) {
        fscanf(fA, " %d", A+i);
        fscanf(fB, " %d", B+i);
    }
    fclose(fB);
    fclose(fA);

    inicio = omp_get_wtime();
    #pragma omp parallel for shared(A,B,C) private(i)
    for(i=0; i<nA*nA; i++)
        C[i] = A[i] + B[i];
    fim = omp_get_wtime();
    t = fim-inicio;

    fC = fopen("C_openmp", "w");
    fprintf(fC, "%d", nA);
    for(i=0; i<nA*nA; i++)
        fprintf(fC, " %d", C[i]);
    fprintf(fC, " %lf", t);
    fclose(fC);

    return 0;
}