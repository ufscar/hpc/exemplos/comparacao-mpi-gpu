#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <mpi.h>

int main(int argc, char* argv[]) {
    double inicio, fim;
    double t;
    int rank, size;
    int nA, nB;
    int *A, *B, *C;
    int i, i_sent, i_recv;
    FILE *fC;
    FILE *fA = fopen("A", "r");
    FILE *fB = fopen("B", "r");
    fscanf(fA, " %d", &nA);
    fscanf(fB, " %d", &nB);
    if(nA != nB) {
        printf("ERRO: matrizes geradas incorretamente\n");
        exit(1);
    }
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // inicializa as matrizes A e B
    if(rank == 0) {
        A = (int*) malloc(nA*nA*sizeof(int));
        B = (int*) malloc(nA*nA*sizeof(int));
        C = (int*) malloc(nA*nA*sizeof(int));
        for(i=0; i<nA*nA; i++) {
            fscanf(fA, " %d", A+i);
            fscanf(fB, " %d", B+i);
        }
        fclose(fB);
        fclose(fA);
    } else {
        fclose(fB);
        fclose(fA);
        A = (int*) malloc(nA*sizeof(int));
        B = (int*) malloc(nA*sizeof(int));
        C = (int*) malloc(nA*sizeof(int));
    }

    // Calcula a soma das matrizes
    inicio = MPI_Wtime();
    if(rank == 0) {
        while(i_recv < nA*nA) {
            for(i = 1; i < size && i_sent < nA*nA; i++) {
                MPI_Send(A + i_sent, nA, MPI_INT, i, 0, MPI_COMM_WORLD);
                MPI_Send(B + i_sent, nA, MPI_INT, i, 0, MPI_COMM_WORLD);
                i_sent += nA;
            }
            for(i = 1; i < size && i_recv < nA*nA; i++) {
                MPI_Recv(C + i_recv, nA, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                i_recv += nA;
            }
        }
        A[0] = B[0] = INT_MAX;
        for(i = 1; i < size; i++) {
            MPI_Send(A, nA, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Send(B, nA, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
    } else { // workers
        while(1) {
            MPI_Recv(A, nA, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(B, nA, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            if(A[0] == INT_MAX && B[0] == INT_MAX)
                break;
            for(i=0; i<nA; i++)
                C[i] = A[i] + B[i];
            MPI_Send(C, nA, MPI_INT, 0, 0, MPI_COMM_WORLD);
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);

    fim = MPI_Wtime();
    t = fim-inicio;

    // Imprime a matriz resultante
    if (rank == 0) {
        fC = fopen("C_mpi", "w");
        fprintf(fC, "%d", nA);
        for(i=0; i<nA*nA; i++)
            fprintf(fC, " %d", C[i]);
        fprintf(fC, " %lf", t);
        fclose(fC);
    }

    MPI_Finalize();

    return 0;
}