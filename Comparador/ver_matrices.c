#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    if(argc < 2) {
        printf("USAGE: %s <arquivo>\n", argv[0]);
        printf("O arquivo deve estar no mesmo formato que o de entrada com um adicional do tempo de execução em segundos\n", argv[0]);
        exit(1);
    }
    FILE *fp = fopen(argv[1], "r");
    FILE *fC = fopen("C", "r");
    int i;
    int n, a, c;
    double t;
    fscanf(fp, " %d", &a);
    fscanf(fC, " %d", &n);
    if(n != a) {
        printf("ERRO: o arquivo %s está incorreto!\n", argv[1]);
        return 0;
    }
    for(i=0; i<n*n; i++) {
        if(fscanf(fp, " %d", &a) <= 0) {
            printf("ERRO: o arquivo %s está incorreto!\n", argv[1]);
            return 0;
        }
        fscanf(fC, " %d", &c);
        if(a != c) {
            printf("ERRO: o arquivo %s está incorreto!\n", argv[1]);
            return 0;
        }
    }
    fclose(fC);
    fscanf(fp, " %lf", &t);
    fclose(fp);

    printf("%s: resultado correto e tempo de execução %lf s\n", argv[1], t);
    return 0;
}