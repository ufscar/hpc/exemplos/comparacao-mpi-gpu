#include <stdio.h>
#include <stdlib.h>
#include <time.h>
 
#define M 10000000

int main(int argc, char *argv[]) {
    // C = A + B
    int i,j;
    int a, b;
    int N;
    FILE *fA, *fB, *fC;
    N = argc > 1 ? atoi(argv[1]) : 1024;
    srand(time(NULL));
    fA = fopen("A", "w");
    fprintf(fA, "%d", N);
    fB = fopen("B", "w");
    fprintf(fB, "%d", N);
    fC = fopen("C", "w");
    fprintf(fC, "%d", N);
    for(i=0; i<N; i++)
        for(j=0; j<N; j++) {
            a = rand() % M;
            b = rand() % M;
            fprintf(fA, " %d", a);
            fprintf(fB, " %d", b);
            fprintf(fC, " %d", a+b);
        }
    fclose(fC);
    fclose(fB);
    fclose(fA);
    return 0;
}
