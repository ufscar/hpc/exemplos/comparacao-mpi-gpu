#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    clock_t inicio, fim;
    double t;
    int nA, nB;
    int *A, *B, *C;
    int i, j;
    int size;
    FILE *fC;
    FILE *fA = fopen("A", "r");
    FILE *fB = fopen("B", "r");
    fscanf(fA, " %d", &nA);
    fscanf(fB, " %d", &nB);
    if(nA != nB) {
        printf("ERRO: matrizes geradas incorretamente\n");
        exit(1);
    }

    size = nA*nA*sizeof(int);

    // inicializa as matrizes A e B
    A = (int*) malloc(size);
    B = (int*) malloc(size);
    C = (int*) malloc(size);
    for(i=0; i<nA*nA; i++) {
        fscanf(fA, " %d", A+i);
        fscanf(fB, " %d", B+i);
    }
    fclose(fB);
    fclose(fA);

    inicio = clock();
    for(i=0; i<nA*nA; i++)
        C[i] = A[i] + B[i];
    fim = clock();
    t = ((double) (fim - inicio)) / CLOCKS_PER_SEC;

    fC = fopen("C_seq", "w");
    fprintf(fC, "%d", nA);
    for(i=0; i<nA*nA; i++)
        fprintf(fC, " %d", C[i]);
    fprintf(fC, " %lf", t);
    fclose(fC);

    return 0;
}