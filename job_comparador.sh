#!/bin/bash
#SBATCH -J comparador             # Job name
#SBATCH -o %j.out                 # Name of stdout output
#SBATCH -w c11                    # Node requested
#SBATCH --partition=fast
#SBATCH --ntasks=35                # Total number of processors
#SBATCH --time=01:30:00           # Run time (hh:mm:ss) - 1.5 hours
#SBATCH --cpus-per-task=1         # Number of CPUs per task
#SBATCH --gres=gpu:turing:1       # GPU
#SBATCH --mem=100G
#SBATCH --mail-user=afsmaira@ufscar.br
#SBATCH --mail-type=ALL

umask 077

singExec="singularity exec --bind=/scratch:/scratch --bind=/var/spool/slurm:/var/spool/slurm"
singRun="singularity run --bind=/scratch:/scratch --bind=/var/spool/slurm:/var/spool/slurm"
justOne="srun --ntasks=1 --ntasks-per-node=1 --cpus-per-task=1";
singGer="${justOne} ${singExec} gerencia.sif"
singSEQ="${justOne} ${singRun} gerencia.sif";
singOMP="srun --ntasks=1 --cpus-per-task=1 ${singRun} openmp.sif";
singMPI="srun --ntasks=35 --cpus-per-task=1 ${singRun} mpi.sif";
singGPU="${justOne} --gres=gpu:turing:1 ${singRun} --nv gpu.sif";

curr=$(pwd)
job_folder="/scratch/job_${SLURM_JOB_ID}"
$justOne mkdir -p "${job_folder}"

function clean() {
  echo "Limpando ambiente..."
  $justOne cp "${job_folder}/C"* "${curr}/"
  rm -rf "${job_folder}";
}
trap clean EXIT HUP INT TERM ERR

set -eE

# shellcheck disable=SC2045
for f in $( ls containers/ );
do
  sbcast -v "containers/${f}" "${job_folder}/${f}";
done;

# shellcheck disable=SC2164
cd "${job_folder}";

echo "Criando matrizes..."
$singGer /opt/gera 128

echo "Executando programa sequencial..."
$singSEQ

echo "Executando programa OpenMP..."
$singOMP

echo "Executando programa MPI..."
$singMPI

echo "Executando programa CUDA..."
$singGPU

echo "Comparando resultados..."
$singGer /opt/verifica C_seq
$singGer /opt/verifica C_openmp
$singGer /opt/verifica C_mpi
$singGer /opt/verifica C_gpu
