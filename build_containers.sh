#!/bin/bash

mkdir -p containers || true
echo "1) Criando container de gerência e programa sequencial..."
[ -f containers/gerencia.sif ] || singularity build containers/gerencia.sif Singularity/Singularity_gerencia
[ -f containers/gerencia.sif ] || exit 1;
echo "2) Criando container de OpenMP..."
[ -f containers/openmp.sif ] || singularity build containers/openmp.sif Singularity/Singularity_openmp
[ -f containers/openmp.sif ] || exit 1;
echo "3) Criando container de MPI..."
[ -f containers/mpi.sif ] || singularity build containers/mpi.sif Singularity/Singularity_MPI
[ -f containers/mpi.sif ] || exit 1;
echo "4) Criando container de GPU..."
[ -f containers/gpu.sif ] || singularity build containers/gpu.sif Singularity/Singularity_GPU
[ -f containers/gpu.sif ] || exit 1;

chown -R afsmaira:afsmaira containers